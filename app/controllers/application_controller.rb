class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  # before_action :allow_cross_domain_access
  # after_action :cors_set_access_control_headers
  # skip_before_filter :verify_authenticity_token
end
