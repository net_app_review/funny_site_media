module Api::V1
  class VideosController < ApiController
    require 'streamio-ffmpeg'
    require 'google/cloud/storage'
    require 'rest-client'

    before_action :set_video, only: [:show, :update]

    PAGINATION_NUMBER = 5

    # GET /videos
    def index
      if current_user
        @videos = video.where(status: 'approved')
                           .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                           .order('created_at DESC')#.to_json(:include =>{:user=>{:only => [:id, :name]}, :comments => {:only =>[:id]}})
      else
        @videos = video.where(status: 'approved', sensitive: false)
                           .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                           .order('created_at DESC')

        if params[:page].to_i > 1
          @videos = [*@videos].push(get_sensitive_video)
        end
      end
      render json: @videos, each_serializer: videoSerializer
    end

    # GET /videos/1
    def show
      if @video.view == nil
        @video.view = 1
      else
        @video.view = @video.view + 1
      end
      @video.user.increment!(:points, 1)
      @video.save
      # render json: @video.to_json(:include =>{:category=>{:only => [:id, :name, :icon]}})
      render json: @video, serializer: SinglevideoSerializer
    end

    # POST /videos
    def create
      # if checkUser(request.headers['X-User-Token']) == true

      if checkUser(request.headers['X-User-Token']) == true
        FFMPEG.ffmpeg_binary = '/usr/bin/ffmpeg'

        # uploader = PostVideoUploader.new
        # uploader.store!(video_params[:video_file])
        # formatted_movie = movie.transcode(Dir.pwd + '/public/uploads/tmp' + 'pro1.mp4', options)
        # uploader.remove!
        # puts movie.duration
        @video = Video.new(video_params)

        if @video.save
          # movie = FFMPEG::Movie.new(Dir.pwd + '/public' + uploader.url)

          movie = FFMPEG::Movie.new(Dir.pwd + '/public' + @video.videofile.url)

          if !movie.video_codec.eql? 'h264'
            render json: {
              messages: 'video codec not acceptable',
              is_success: false,
              data: {
                video_codec: movie.video_codec
              }
            }, status: :not_acceptable
          else
            options = {
              video_codec: 'libx264',
              frame_rate: 10,
              resolution: "480x240",
              video_bitrate: 300,
              video_bitrate_tolerance: 100,
              aspect: 1.33333333,
              keyframe_interval: 90,
              x264_vprofile: 'high', x264_preset: 'slow',
              audio_codec: movie.audio_codec,
              audio_bitrate: 32,
              audio_sample_rate: 22050,
              audio_channels: 1
              # custom: %w()
            }

            movie.transcode(Dir.pwd + '/public/uploads/tmp/' + @video.videofile.filename, options)



            # movie.transcode(@video.videofile.file.file, options) { |progress| puts progress }

            # movie.transcode(@video.videofile.file.file, %w(-c:v libx265 -an -x265-params crf=25 OUT.mov))                      

            # project_id = ENV['AWS_ACCESS_KEY_ID']
            # @video.videofile.remove!
            # Set up google bucket
            storage = Google::Cloud::Storage.new(
              project_id: 'funniq-78dbd',
              credentials: "#{Dir.pwd}/config/secrets/funniq-78dbd-dbdf0b3239df.json"
            )
            bucket = storage.bucket 'funniq-78dbd.appspot.com'

            # Process the thumbnail

            thumbnail_filename = "#{Dir.pwd}/#{removeExtentionFromFile(@video.videofile.filename)}.jpg"
            movie.transcode(thumbnail_filename, %w(-ss 00:00:01 -vframes 1 -f image2))
            thumbnail_firebase = bucket.create_file thumbnail_filename, "#{removeExtentionFromFile(@video.videofile.filename)}.jpg", content_type: "image/jpg"
            thumbnail_firebase.acl.public!
            thumbnail_firebase_encoded = ERB::Util.url_encode(thumbnail_firebase.name)
            thumbnail_uuid = SecureRandom.uuid
            thumbnail_public_url = "https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + thumbnail_firebase_encoded + "?alt=media&token=" + thumbnail_uuid
            File.delete(thumbnail_filename) if File.exist?(thumbnail_filename)
            # thumbnail_filename.remove!

            # Process the video to firebase
            uuid = SecureRandom.uuid
            file = bucket.create_file movie.path, "funniq_development/#{@video.videofile.filename}", content_type: "video/mp4"
            file_encoded = ERB::Util.url_encode(file.name)
            file.acl.public!
            public_url = "https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + file_encoded + "?alt=media&token=" + uuid

            @video.videofile.remove!

            render json: {
              status: 200,
              public_url: public_url,
              thumbnail_url: thumbnail_public_url,
              msg: ' The image is successfully uploaded!!'
            }
          end
        else
          render json: { user: @video.errors }, status: unprocessable_entity
        end
      else
        render json: {
          messages: 'Unauthenticated',
          is_success: false,
          data: {}
        }, status: :unauthorized
      end
    end

    def removeExtentionFromFile (fileName)
      fileName.sub(/\.[^.]+\z/, '')
    end

    def checkUser(token)
      # header: AAFtj2pmYtTQytWpq43RMtp6
      is_user = false

      # url = 'http://localhost:5054/api/v1/users/current_user'

      url = ENV['FUNNIQ_BACKEND_URL'] + '/api/v1/users/current_user'

      begin
        response = RestClient.get(url, headers = {'x-user-token': token})

        id = JSON.load(response.body)['current_user']['id']

        if !id.nil?
          is_user = true
        end
      rescue StandardError => e
        puts e
        is_user = false
      end
      is_user
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = video.find(params[:id])
    end


    # Only allow a trusted parameter "white list" through.
    def video_params
      # params.require(:video).permit(:video_file)
      # params.permit(:video, :videofile)
      params.permit(:videofile)
      # params.permit(:videofile)
    end
  end
end