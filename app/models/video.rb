class Video < ApplicationRecord
  mount_uploader :videofile, PostVideoUploader

  def video_path
    # Serve the raw video until the processing is done.
    if video_processing
      video.url
    else
      video.rescaled.url
    end
  end
end
