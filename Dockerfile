FROM ruby:2.5.3

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install -y ffmpeg

RUN apt-get update && apt-get install -y nodejs mysql-client sqlite3 vim --no-install-recommends && rm -rf /var/lib/apt/lists/*
# RUN apt-get install libmysqlclient-dev
ENV SECRET_KEY_BASE e20323af41a6c8a787bd046039a865bc64d53422a4a0ff432068ed66dd4c92ac6b43b1fed40ae7d8384222147fe190187c58c900b3b5e5de348630498f191e8f

ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/

# RUN gem install bcrypt -v '3.1.13'
# RUN gem install ffi -v '1.11.1'
# RUN gem install websocket-driver -v '0.6.5' 
# RUN gem install nio4r -v '2.3.1' --source 'https://rubygems.org/'
# RUN gem install nokogiri -v '1.10.3' --source 'https://rubygems.org/'
# RUN gem uninstall bundler
# RUN gem install bundler
# RUN gem update --system
# RUN bundle config --global frozen 1
# RUN bundle install --without development test

RUN bundle install

COPY . /usr/src/app
# RUN bundle exec rake

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]