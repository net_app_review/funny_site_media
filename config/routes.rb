Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  resources :videos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :videos do
        collection do
          post 'create', to: 'videos#create'
          post 'update', to: 'videos#update'
        end
      end
    end
  end
end
