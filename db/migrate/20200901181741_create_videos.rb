class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string :name
      t.string :url
      t.integer :width
      t.integer :height
      t.string :thumbnail1
      t.string :thumbnail2

      t.timestamps
    end
  end
end
